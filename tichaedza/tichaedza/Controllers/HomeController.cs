﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using tichaedza.DAL;
using tichaedza.DAL.security;
using tichaedza.Models;
using Database = WebMatrix.Data.Database;
namespace tichaedza.Controllers
{
    public class HomeController : Controller
    {
        private GreenContext db = new GreenContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Services()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Register()
        {
            ViewBag.SuccessMsg = "";
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserId,Username,Password,ConfirmPassword,FirstName,LastName")] User user)
        {
            user.Password = "A";
            user.ConfirmPassword = "A";
            user.FirstName = "A";
            user.LastName= "A";
           string num = Convert.ToString(Request["Username"].ToString());
            //check if customer exsists
       var user5= db.Users.ToList().Where(a => a.Username==num);
            string compare = "";
            foreach (var q in user5)
            {
                compare = q.Username;
            }
            string code22 = "";
            ViewBag.Message = "Register";
            //now do the HTML formatting
            if (Request["Username"].ToString()== compare)
            {
                ViewBag.SuccessMsg = "";
                var mod3 = ModelState.First(c => c.Key == "Username"); // this
                mod3.Value.Errors.Add("Customer Number exsists");
                return View(user);
            }//now append it to the body of the mail
            else if (Request["Username"].ToString()!="" && Request["Username"].ToString()!=compare)
            {
                //db.Contacts.Add(contact);
                var con2 = Database.Open("GreenContext");
                String broker33 = Convert.ToString(Request["Username"]);
                var brokerCode = "SELECT * FROM CUSTOMER_DETAILS where CUSTOMER_NUMBER='" + broker33 + "'";
                var list12 = con2.Query(brokerCode).ToList();
           
                foreach (var row in list12)
                {
                    code22 = row.CUSTOMER_NUMBER;
                }


             if (code22==broker33)
                {
                    //  await db.SaveChangesAsync();
                
                    
                    user.Username = code22;
                   user.Password = ComputeHash("123456", new SHA256CryptoServiceProvider());
                    user.ConfirmPassword =user.Password;
                    user.LastName = "B";
                   
                    //Insert Blank Row in Table  
                    db.Users.Add(user);

                    //Update table  
                    db.SaveChanges();
                    string fullname = "User";
                    var role = db.Roles.ToList().Where(a => a.RoleName == fullname);


                    int roleid = 0;
                    int userid = 0;
                    foreach (var row in role)
                    {
                        roleid = row.RoleId;
                    }
                    int user2 = db.Users.Max(a => a.UserId);

                    userid = user2;
                    string cs = ConfigurationManager.ConnectionStrings["GreenContext"].ConnectionString;

                    using (SqlConnection cn = new SqlConnection(cs))
                    {
                        string sql =
                            "INSERT INTO UserRoles (UserId,RoleId) VALUES (@UserId,@RoleId)";
                        SqlCommand cmd = new SqlCommand(sql);
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = cn;
                        cmd.Parameters.AddWithValue("@UserId", userid);
                        cmd.Parameters.AddWithValue("@RoleId", roleid);

                        cn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            cn.Close();
                        }
                        catch (Exception)
                        {

                            throw;
                        }

                    }
                    ViewBag.SuccessMsg = "Thank you for registering with us we have sent an sms";
                ModelState.Clear();
                return View();
                }
               
            }



            ViewBag.SuccessMsg = "";
            var mod2 = ModelState.First(c => c.Key == "Username"); // this
            mod2.Value.Errors.Add("Customer Number is required or incorrect");
            return View(user);
        }

        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }


    }
}
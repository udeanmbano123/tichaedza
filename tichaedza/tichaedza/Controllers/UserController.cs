﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using tichaedza.DAL;
using tichaedza.DAL.security;
using tichaedza.Models;
using WebMatrix.Data;
using WebMatrix.WebData;

namespace tichaedza.Controllers
{    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
     // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class UserController : Controller
    {
        private GreenContext db = new GreenContext();
        // GET: User
        public ActionResult Index()
        {
            ViewBag.Title = "Index";
            return View();
        }

        public ActionResult Balance(int? page)
        {
            
            Database con2;

            var con = Database.Open("GreenContext");

            var account = User.Identity.Name;
            //var selectQueryString = "SELECT Category,SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account='" + account + "' GROUP BY Category";

            //  var selectQueryString= "SELECT isnull((select Acctype_description from para_acctype where Acctype = Accounts_Transactions.Category ), Category) as (Description]),SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account = '"+ account + "'GROUP BY Category";
            var selectQueryString = "SELECT isnull((select Acctype_description from para_acctype where Acctype = Accounts_Transactions.Category ), Category) as Description, isNull(SUM(Amount) , '0.00') as 'TotalAmount' FROM Accounts_Transactions where Account = '" + account + "'GROUP BY Category";
            try
            {
                var products = con.Query(selectQueryString);
                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
                var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                var onePageOfProducts = products.ToPagedList(pageNumber, 20); // will only contain 25 products max because of the pageSize

                ViewBag.OnePageOfProducts = onePageOfProducts;

               // ViewBag.MemberNumber = memberno;
                //ViewBag.Username = username;
                //var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                //var onePageOfProducts = selectQueryString.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

                //ViewBag.OnePageOfProducts = onePageOfProducts;
                con.Connection.Close();
                con.Close();
            }
            catch (Exception e)
            {
                return RedirectToAction("Balance");
            }

            return View();
        }


        public ActionResult ApplicationTracker(int? page)
        {

            Database con2;

            var con = Database.Open("GreenContext");

            var account = User.Identity.Name;
            //var selectQueryString = "SELECT Category,SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account='" + account + "' GROUP BY Category";

            //  var selectQueryString= "SELECT isnull((select Acctype_description from para_acctype where Acctype = Accounts_Transactions.Category ), Category) as (Description]),SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account = '"+ account + "'GROUP BY Category";
            var selectQueryString = "SELECT * from QUEST_APPLICATION where CUSTOMER_NUMBER='" + account + "'";
            try
            {
                var products = con.Query(selectQueryString);
                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
                var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                var onePageOfProducts = products.ToPagedList(pageNumber, 20); // will only contain 25 products max because of the pageSize

                ViewBag.OnePageOfProducts = onePageOfProducts;

                // ViewBag.MemberNumber = memberno;
                //ViewBag.Username = username;
                //var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                //var onePageOfProducts = selectQueryString.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

                //ViewBag.OnePageOfProducts = onePageOfProducts;
                con.Connection.Close();
                con.Close();
            }
            catch (Exception e)
            {
                return RedirectToAction("Balance");
            }

            return View();
        }



        public ActionResult LoanTracker(int? page,LoanTracker loan)
        {

            Database con2;

            
                      var con = Database.Open("GreenContext");
            var referencenumber = "";
            try
            {
            referencenumber = Request["reference"].ToString();
            }
            catch (Exception)
            {

                referencenumber = "";
            }

            
            var account = User.Identity.Name;
            //var selectQueryString = "SELECT Category,SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account='" + account + "' GROUP BY Category";

            //  var selectQueryString= "SELECT isnull((select Acctype_description from para_acctype where Acctype = Accounts_Transactions.Category ), Category) as (Description]),SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account = '"+ account + "'GROUP BY Category";
            var selectQueryString = "SELECT id,txn_code,description,amount from trans where reference= '" + referencenumber + "'";
            try
            {
                var products = con.Query(selectQueryString);
                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
                var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                var onePageOfProducts = products.ToPagedList(pageNumber, 20); // will only contain 25 products max because of the pageSize

                ViewBag.OnePageOfProducts = onePageOfProducts;

                // ViewBag.MemberNumber = memberno;
                //ViewBag.Username = username;
                //var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                //var onePageOfProducts = selectQueryString.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

                //ViewBag.OnePageOfProducts = onePageOfProducts;
                con.Connection.Close();
                con.Close();
            }
            catch (Exception e)
            {
                return RedirectToAction("Balance");
            }

            return View();
            }

        public ActionResult Nominees(int? page)
        {

            Database con2;

            var con = Database.Open("GreenContext");

            var account = User.Identity.Name;
            //var selectQueryString = "SELECT Category,SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account='" + account + "' GROUP BY Category";

            //  var selectQueryString= "SELECT isnull((select Acctype_description from para_acctype where Acctype = Accounts_Transactions.Category ), Category) as (Description]),SUM(Amount) AS TotalAmount FROM Accounts_Transactions where Account = '"+ account + "'GROUP BY Category";
            var selectQueryString = "SELECT Name,ID_Number,DOB,Relationship from sacco_NOMINEE where Customer_No= '" + account + "'";
            try
            {
                var products = con.Query(selectQueryString);
                //returns IQueryable<Product> representing an unknown number of products. a thousand maybe?
                var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                var onePageOfProducts = products.ToPagedList(pageNumber, 20); // will only contain 25 products max because of the pageSize

                ViewBag.OnePageOfProducts = onePageOfProducts;

                // ViewBag.MemberNumber = memberno;
                //ViewBag.Username = username;
                //var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
                //var onePageOfProducts = selectQueryString.ToPagedList(pageNumber, 25); // will only contain 25 products max because of the pageSize

                //ViewBag.OnePageOfProducts = onePageOfProducts;
                con.Connection.Close();
                con.Close();
            }
            catch (Exception e)
            {
                return RedirectToAction("Nominees");
            }

            return View();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CompareObsolete = System.Web.Mvc.CompareAttribute;
namespace tichaedza.Models
{
    public class User
    {   [Key]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Customer Number is required or incorrect")]
        public String Username { get; set; }

      [Required]
        public String Password { get; set; }
        [Display(Name = "Confirm password")]
        [Required]
        [CompareObsolete("Password", ErrorMessage = "The password and confirmation password do not match.")]

        public String ConfirmPassword { get; set; }
 
        public String FirstName { get; set; }
   public String LastName { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
    }
}
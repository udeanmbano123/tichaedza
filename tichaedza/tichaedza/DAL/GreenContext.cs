﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using tichaedza.Models;

namespace tichaedza.DAL
{
    public class GreenContext : DbContext
    {
        public GreenContext() : base("GreenContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //one-to-many relationships
            modelBuilder.Entity<User>()
               .HasMany(u => u.Roles)
               .WithMany(r => r.Users)
               .Map(m =>
               {
                   m.ToTable("UserRoles");
                   m.MapLeftKey("UserId");
                   m.MapRightKey("RoleId");
               });
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }



        //  public System.Data.Entity.DbSet<investorpage.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}